#!/bin/bash -xe
export namespace=serverless-hello-world 
kubeless function update get-nodejs --handler helloget.requestWorld --from-file ../src/helloget.js --namespace=${namespace}

