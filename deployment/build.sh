#!/bin/bash -xe

#https://github.com/kubeless/kubeless/releases/download/v1.0.0-alpha.5/kinesis-v1.0.0-alpha.5.yaml
export RELEASE=v1.0.0-alpha.5
kubectl create ns kubeless
kubectl create -f https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless-$RELEASE.yaml