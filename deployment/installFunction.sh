#!/bin/bash -xe
export namespace=serverless-hello-world 
kubectl create namespace ${namespace} || true
kubectl create -f prod-cert.yaml --namespace=${namespace} || true
kubeless function deploy get-nodejs --runtime nodejs6 --handler helloget.requestWorld --from-file ../src/helloget.js --namespace=${namespace}
kubeless trigger http create get-nodejs-triger --function-name get-nodejs --hostname prod.devtrails.co.nz \
                             --path serverless/hello --tls-secret prod-devtrails-crt-secret --namespace=${namespace}
